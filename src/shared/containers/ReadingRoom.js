import React from 'react';
import { translate } from 'react-i18next';

import B_Tabs from '../components/B_Tabs';
import Fonds from '../components/ReadingRoomPages/Fonds';
import ReadingRoomTable from '../components/ReadingRoomPages/ReadingRoomTable';
import Inventories from '../components/ReadingRoomPages/Inventories';
import Cases from '../components/ReadingRoomPages/Cases';
import { Tabs, Table, Icon, Button } from 'antd';


const columns = [{
  title: 'Название',
  dataIndex: 'name',
  render: text => <a href="javascript:;">{text}</a>,
}, {
  title: 'Дата создание',
  dataIndex: 'age',
}, {
  title: 'Доп инфо',
  dataIndex: 'address',
}];


const dataFunds = [{
  key: 1,
  name: 'Фонд 1',
  age: '16.08.2018',
  address: 'New York No. 1 Lake Park',
}, {
  key: 2,
  name: 'Фонд 2',
  age: '16.08.2018',
  address: 'London No. 1 Lake Park',
}, {
  key: 3,
  name: 'Фонд 3',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 4,
  name: 'Фонд 4',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}];

const dataInv = [{
  key: 1,
  fundsId: 1,
  name: 'Inventory 1',
  age: '16.08.2018',
  address: 'New York No. 1 Lake Park',
}, {
  key: 2,
  fundsId: 1,
  name: 'Inventory 2',
  age: '16.08.2018',
  address: 'London No. 1 Lake Park',
}, {
  key: 3,
  fundsId: 2,
  name: 'Inventory 3',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 4,
  fundsId: 3,
  name: 'Inventory 4',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 5,
  fundsId: 3,
  name: 'Inventory 5',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 6,
  fundsId: 3,
  name: 'Inventory 6',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 7,
  fundsId: 4,
  name: 'Inventory 7',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: 8,
  fundsId: 4,
  name: 'Inventory 8',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}];

const dataCases = [{
  key: '1',
  invId: 1,
  name: 'Case 1',
  age: '16.08.2018',
  address: 'New York No. 1 Lake Park',
}, {
  key: '2',
  invId: 1,
  name: 'Case 2',
  age: '16.08.2018',
  address: 'London No. 1 Lake Park',
}, {
  key: '3',
  invId: 1,
  name: 'Case 3',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '4',
  invId:3,
  name: 'Case 4',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '5',
  invId: 3,
  name: 'Case 5',
  age: '16.08.2018',
  address: 'London No. 1 Lake Park',
}, {
  key: '6',
  invId: 4,
  name: 'Case 6',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '7',
  invId: 4,
  name: 'Case 7',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '8',
  invId: 4,
  name: 'Case 8',
  age: '16.08.2018',
  address: 'London No. 1 Lake Park',
}, {
  key: '9',
  name: 'Case 9',
  invId: 7,
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}, {
  key: '10',
  invId: 7,
  name: 'Case 10',
  age: '16.08.2018',
  address: 'Sidney No. 1 Lake Park',
}];

const FUNDS = 'FUNDS'
const INV = 'INVENTORIES'
const CASES = 'CASES'


class ReadingRoom extends React.Component {

  state = {
    funds: dataFunds,
    inventories: [],
    cases: [],
    selectedFunds: [],
    selectedInv: [],
    selectedCases: [],
    activeTab: 0,
    loading: false,
  };

  callback(key) {
    console.log(key);
  }

  onSelectChangeFunds = (selectedRowKeys) => {
    let inv = dataInv.filter( item => selectedRowKeys.includes(item.fundsId))
    this.setState({
      inventories: inv,
      cases: [],
      selectedFunds: selectedRowKeys,
      selectedInv: [],
      selectedCases: []
    })
  }

  onSelectChangeInv = (selectedRowKeys) => {
    let cases = dataCases.filter( item => selectedRowKeys.includes(item.invId))
    this.setState({
      cases: cases,
      selectedInv: selectedRowKeys,
      selectedCases: []
    })
  }

  onSelectChangeCases = (selectedRowKeys) => {
    this.setState({ selectedCases: selectedRowKeys })
  }

  updateAll = (type) => {
    if(type === FUNDS) {
      this.setState({ inventories: [], cases: []})
    } else {
      this.setState({ cases: []})
    }
  }

  render() {
    const { activeTab, loading, funds, inventories, cases, selectedFunds, selectedInv, selectedCases} = this.state;

    const TabPane = Tabs.TabPane;

    return (
      <div>
        <Tabs defaultActiveKey="1" onChange={this.callback}>
            <TabPane tab="Funds" key="1">
              <ReadingRoomTable data={funds} type={FUNDS} onSelectChange={this.onSelectChangeFunds}
                updateAll={this.updateAll} selected={selectedFunds}/>
            </TabPane>
            <TabPane tab="Inventories" key="2">
              <ReadingRoomTable data={inventories} type={INV} onSelectChange={this.onSelectChangeInv}
                updateAll={this.updateAll} selected={selectedInv}/>
            </TabPane>
            <TabPane tab="Cases" key="3">
              <ReadingRoomTable data={cases} type={CASES} onSelectChange={this.onSelectChangeCases}
              updateAll={this.updateAll} selected={selectedCases}/>
            </TabPane>
          </Tabs>
      </div>
    );
  }
}

export default translate('readingRoom')(ReadingRoom);
