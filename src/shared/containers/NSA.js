import React, { Component } from 'react';
import { translate } from 'react-i18next';
import PropTypes from 'prop-types'
import SearchNSA from '../components/NSAPages/SearchNSA';
import CreateDocument from '../components/NSAPages/CreateDocument';
import {Route, Switch} from 'react-router-dom';
import EditNSAPage from '../components/NSAPages/EditNSAPage';

class NSA extends Component {

  render() {
    const { t, tofiConstants } = this.props;

    return (
      <div className="NSA">
        <h2>Научно-справочный аппарат</h2>
        <Switch>
        <Route exact path="/sra/searchPage" component={props => <SearchNSA t={t} {...props}/> } />
        <Route exact path="/sra/createDocument" component={props => <CreateDocument t={t} {...props}/> } />
        <Route exact path="/sra/createDocument/:type/:id" component={props => <EditNSAPage t={t} {...props} tofiConstants={tofiConstants}/>} />
      </Switch>
      </div>
    )
  }
}

NSA.propTypes = {
  t: PropTypes.func.isRequired,
  tofiConstants: PropTypes.shape().isRequired
};

export default translate('NSA')(NSA);

