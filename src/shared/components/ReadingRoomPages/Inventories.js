import React from 'react';
import { connect } from 'react-redux';
import {Breadcrumb, Input} from 'antd';
import PropTypes from 'prop-types';

import AntTable from '../AntTable';
import {parseCube_new} from '../../utils/cubeParser';
import {getCasesCount, getCube, inventoriesLoaded} from '../../actions/actions';
import {CUBE_FOR_RR, DBEG, DEND, DO_FOR_RR, INVENTNUMB} from '../../constants/tofiConstants';
import {isEmpty} from 'lodash';

const Search = Input.Search;


class Inventories extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      search: '',
      loading: true,
      errors: {},
      countData: {
        countFund: 0,
        countDelo: 0,
        countDeloFile: 0
      }
    }
  }

  componentDidMount() {
    this._isMounted = true;
    const filters = {
      filterDOAnd: [
        {
          dimConst: DO_FOR_RR,
          concatType: "and",
          conds: [
            {
              parents: this.props.fund.key
            }
          ]
        }
      ]
    };
    if(isEmpty(this.props.inventories)) {
        this.setState({loading: true});
        this.props.getCube(CUBE_FOR_RR, JSON.stringify(filters), {customKey: 'CubeRRInv', parent: this.props.fund.key});
    } else if(this.props.inventories.parent !== this.props.fund.key){
        this.setState({loading: true});
        this.props.getCube(CUBE_FOR_RR, JSON.stringify(filters), {customKey: 'CubeRRInv', parent: this.props.fund.key});
    } else {
        const { inventories, tofiConstants: { doForRR, dpForRR } } = this.props;
        this.setState({ loading: false, data: parseCube_new(inventories['cube'], [], 'dp', 'do', inventories[`do_${doForRR.id}`], inventories[`dp_${dpForRR.id}`], `do_${doForRR.id}`, `dp_${dpForRR.id}`).map(this.dataSet) });
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!isEmpty(nextProps.inventories) && !isEmpty(nextProps.tofiConstants)) {
      const { doForRR, dpForRR } = nextProps.tofiConstants;
      this.setState({ loading: false, data: parseCube_new(nextProps.inventories['cube'], [], 'dp', 'do', nextProps.inventories[`do_${doForRR.id}`], nextProps.inventories[`dp_${dpForRR.id}`], `do_${doForRR.id}`, `dp_${dpForRR.id}`).map(this.dataSet) });
    } else {
      this.setState({ loading: false });
    }
    try {
      const ids = nextProps.inventories[`do_${nextProps.tofiConstants.doForRR.id}`].map(dimObj => dimObj.id);
      getCasesCount(JSON.stringify(ids), 'CubeForRR', 'doForRR')
        .then(caseData => {
          this.setState({
            countData: {
              ...this.state.countData,
              countFund: 1,
              countDelo: caseData.cntCases,
              countDeloFile: caseData.cntEO
            }
          })
        })
    } catch (err) {
      console.log(err);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  dataSet = item => {
    const inventNumbObj = item.props.find(element => element.prop === INVENTNUMB),
          dendObj = item.props.find(element => element.prop === DEND),
          dbegObj = item.props.find(element => element.prop === DBEG);
    return {
      key: item.id,
      inventNumb: !!inventNumbObj ? inventNumbObj.value : '',
      inventName : item.name[localStorage.i18nextLng],
      dend: !!dendObj ? dendObj.value : '',
      dbeg: !!dbegObj ? dbegObj.value: '',
      inventInfo: ''
    }
  };

  changeState = (record, openedBy) => {
    this.props.nextState(record, openedBy)
  };

  searchUpdate = e => {
    this.setState({ search: e.target.value })
  };

  renderTableFooter = () => {
    const {data, countData: {countFund, countDelo, countDeloFile}} = this.state;
    return (
      <div className="table-footer">
        <div className="flex">
          <div className="label"><label htmlFor="">Фондов:</label><Input size='small' type="text" readOnly value={countFund}/></div>
          <div className="label"><label htmlFor="">Описей:</label><Input size='small' type="text" readOnly value={data.length}/></div>
          <div className="label"><label htmlFor="">Дел:</label><Input size='small' type="text" readOnly value={countDelo}/></div>
          <div className="label"><label htmlFor="">Дел с электронными образами:</label><Input size='small' type="text" readOnly value={countDeloFile}/></div>
        </div>
        <div className="data-length">
          <div className="label"><label htmlFor="">Всего:</label><Input size='small' type="text" readOnly value={this.filteredData.length + ' / ' + this.state.data.length}/></div>
        </div>
      </div>
    )
  };

  render() {
    const { data, loading, search } = this.state;
    const { t } = this.props;
    this.filteredData = data.filter(item =>
      (
        item.inventName.toLowerCase().includes(search.toLowerCase()) ||
        item.inventNumb.includes(search.toLowerCase()) ||
        item.dend.toLowerCase().includes(search.toLowerCase()) ||
        item.dbeg.toLowerCase().includes(search.toLowerCase())
      )
    );
    return (
      <div className="Inventories">
        <div className="Inventories__header">
          <Breadcrumb>
            <Breadcrumb.Item><a role="button" tabIndex={0} data-index="0" onClick={this.props.handleTapClick}>{ t('FUNDS') }</a></Breadcrumb.Item>
            <Breadcrumb.Item>{this.props.fund.fundName}</Breadcrumb.Item>
          </Breadcrumb>
          <Search onChange={this.searchUpdate}/>
        </div>
        <div className="Inventories__body">
          <AntTable
            openedBy='Inventories'
            nextState={this.changeState}
            loading={ loading }
            columns={
              [
                {
                  key: 'inventNumb',
                  title: '№ описи',
                  dataIndex: 'inventNumb',
                  width: '15%'
                },
                {
                  key: 'inventName',
                  title: 'Название описи',
                  dataIndex: 'inventName',
                  width: '21%'
                },
                {
                  key: 'dbeg',
                  title: 'Начальная дата',
                  dataIndex: 'dbeg',
                  width: '21%'
                },
                {
                  key: 'dend',
                  title: 'Конечная дата',
                  dataIndex: 'dend',
                  width: '21%'
                },
                {
                  key: 'inventInfo',
                  title: 'Информация об описи',
                  dataIndex: 'inventInfo',
                  width: '21%'
                }
              ]
            }
            dataSource = { this.filteredData }
            footer={ this.renderTableFooter }
          />
        </div>
      </div>
    )
  }
}

Inventories.propTypes = {
  inventoriesLoaded: PropTypes.func.isRequired,
  nextState: PropTypes.func.isRequired,
  inventories: PropTypes.shape(),
  fund: PropTypes.shape({
    key: PropTypes.string.isRequired,
    fundName: PropTypes.string.isRequired
  }).isRequired,
  tofiConstants: PropTypes.shape({
    doForInv: PropTypes.shape(),
    dpForInv: PropTypes.shape()
  }).isRequired
};

function mapStateToProps(state) {
  return {
    inventories: state.cubes['CubeRRInv'],
    tofiConstants: state.generalData.tofiConstants
  }
}

export default connect(mapStateToProps, { inventoriesLoaded, getCube })(Inventories);
