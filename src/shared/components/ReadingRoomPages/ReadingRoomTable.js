import React from 'react';
import { Tabs, Table, Icon, Button } from 'antd';
import { connect } from 'react-redux';


class ReadingRoomTable extends React.Component {


  state = {
    filteredItems: [],
    activeTab: 0,
    selectedRowKeys: [],
    loading: false,
  };

  start = () => {
    this.props.updateAll(this.props.type);
    this.setState({
      selectedRowKeys: [],
    });
  }

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.props.onSelectChange(selectedRowKeys);
    this.setState({ selectedRowKeys });
  }


  render() {
    let { loading, selectedRowKeys } = this.state;
    const {data, selected } = this.props;

    selectedRowKeys = selected;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <div>
        <div>
          <Button
            type="primary"
            onClick={this.start}
            disabled={!hasSelected}
            loading={loading}
            style={{margin: 20}}
          >
            Обновить
          </Button>
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Выбрано ${selectedRowKeys.length} ` : ''}
          </span>
        </div>
        <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
      </div>
    );
  }

}

export default ReadingRoomTable;

const columns = [{
  title: 'Название',
  dataIndex: 'name',
  render: text => <a href="javascript:;">{text}</a>,
}, {
  title: 'Дата создание',
  dataIndex: 'age',
}, {
  title: 'Доп инфо',
  dataIndex: 'address',
}];
