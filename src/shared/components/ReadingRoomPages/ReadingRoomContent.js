import React from 'react';
import PropTypes from 'prop-types';

import Fonds from './Fonds';
import Cases from './Cases';
import Inventories from './Inventories';

const ReadingRoomContent = props => {

  switch (Number(props.activeTab)) {
    case 0: return <Fonds nextState={props.nextState} />;
    case 1: return <Inventories nextState={props.nextState} fundId={props.data.Funds} />;
    case 2: return <Cases inventId={props.data.Inventories} />;
    default: return null;
  }

};

ReadingRoomContent.propTypes = {
  nextState: PropTypes.func.isRequired,
  activeTab: PropTypes.string,
  data: PropTypes.object
};

ReadingRoomContent.defaultProps = {
  activeTap: 0
};

export default ReadingRoomContent;
