import React from 'react';
import {Button, Input, Popconfirm, Icon, Dropdown, Menu, DatePicker} from 'antd';
import Select from 'react-select';
import SelectVirt from "react-virtualized-select";
import {connect} from 'react-redux';
import CSSTransition from 'react-transition-group/CSSTransition'
import moment from 'moment';
import uuid from 'uuid';
import {store} from '../../../../App'

import AntTable from '../../AntTable';
import SiderCard from './SiderCard';
import {isEmpty, isEqual} from 'lodash';
import {
  CUBE_FOR_WORKS, WORK_PRIORITY, WORK_REG_FUND, WORK_STATUS, WORK_ASSIGNED_TO,
  DO_FOR_WORKS, WORK_REG_INV
} from '../../../constants/tofiConstants';
import { WORK_TYPE_ARCHIVE_FUND } from '../../../constants/constants';
import {getCube, getPropVal} from '../../../actions/actions';
import {parseCube_new} from '../../../utils/cubeParser';

/* eslint eqeqeq:0 */
class ArchiveFundWorks extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      priority: [],
      workPriorityLoading: false,
      status: [],
      workStatusLoading: false,
      sourcing: null,
      form: null,
      search: {
        workPlannedStartDate: {
          dbeg: moment().subtract(1, 'w'),
          dend: moment().add(1, 'w')
        },
        workPlannedEndDate: {
          dbeg: null,
          dend: null
        },
        workActualStartDate: {
          dbeg: null,
          dend: null
        },
        workActualEndDate: {
          dbeg: null,
          dend: null
        },
        acceptanceDate: {
          dbeg: null,
          dend: null
        },
        workType: '',
        workRegFund: '',
        workRegInv: '',
        workPriority: '',
        workStatus: '',
        workAssignedTo: ''
      },
      openCard: false,
      selectedRow: null,
      performer: [],
      workAssignedToLoading: false,
      initialValues: {},
      loading: false
    }
  }

  onRadioChange = e => {
    console.log(e.target.value)
  };

  onPriorityChange = s => {this.setState({priority: s})};
  onStatusChange = s => {this.setState({status: s})};
  onSourcingChange = s => {this.setState({sourcing: s})};
  onPerformerChange = s => {this.setState({performer: s})};

  changeSelectedRow = rec => {
    if(isEmpty(this.state.selectedRow) || (!isEqual(this.state.selectedRow, rec) && !this.state.openCard)){
      this.setState({ selectedRow: rec })
    } else {
      const initialValues = {
        key: rec.key,
        workListName: rec.workListNameObj,
        workType: rec.workTypeObj,
        workPlannedEndDate: rec.workPlannedEndDateObj,
        workPriority: rec.workPriority,
        workStatus: rec.workStatusObj,
        workRegFund: rec.workRegFundObj,
        workRegInv: rec.workRegInvObj,
        workAuthor: rec.workAuthor,
        workDate: rec.workDate,
        workAssignedTo: rec.workAssignedTo,
        workPlannedStartDate: rec.workPlannedStartDateObj,
        workActualStartDate: rec.workActualStartDateObj,
        workActualEndDate: rec.workActualEndDateObj,
        acceptanceDate: rec.acceptanceDateObj
      };
      this.setState({ initialValues, openCard: true })
    }
  };

  openCard = () => {
    this.setState({ openCard: true, initialValues: {} })
  };

  closeCard = () => {
    this.setState({ openCard: false })
  };

  onDateChange = (name, dateType) => {
    return date => {
      this.setState({search: {...this.state.search, [name]: {...this.state.search[name], [dateType]: date}}})
    }
  };

  componentDidMount() {
    if(!this.props.works || this.props.worksArchiveTestData.length === 0) {
      this.setState({loading: true});
      const filters = {
        filterDOAnd: [
          {
            dimConst: DO_FOR_WORKS,
            concatType: "and",
            conds: [
              {
                clss: "caseRegistration,caseDisposal,descriptionOfValuableDocs,caseAvailabilityCheck,casesForTemporaryUse,caseExamination,processedCases"
              }
            ]
          }
        ]
      };
      // this.props.getCube(CUBE_FOR_WORKS)
      this.props.getCube(CUBE_FOR_WORKS, JSON.stringify(filters))
        .then(() => {
          Promise.all([this.props.getPropVal(WORK_REG_FUND), this.props.getPropVal(WORK_REG_INV)])
            .then(values => {
              /*this.setState(prevState => ({
                data: prevState.data.map((item, idx) => {
                  const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                  return {
                    ...item,
                    workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                    workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                  }
                })
              }));*/
              store.dispatch({
                type: 'addTestDataArchive',
                testData: this.props.worksArchiveTestData.map((item, idx) => {
                  const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                  const workRegInvObj = this.props.workRegInvOptions ? this.props.workRegInvOptions[idx * 3] : null;
                  return {
                    ...item,
                    workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                    workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                    workRegInv: !!workRegInvObj ? workRegInvObj.name[this.lng] || workRegInvObj.label || '' : '',
                    workRegInvObj: !!workRegInvObj ? {value: workRegInvObj.id, label: workRegInvObj.name[this.lng]} : {}
                  }
                })
              });
              if(!values.every(data => !!data.data)) Promise.all([this.props.getPropVal(WORK_REG_FUND), this.props.getPropVal(WORK_REG_INV)])
                .then(third => {
                  /*this.setState(prevState => ({
                    data: prevState.data.map((item, idx) => {
                      const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                      return {
                        ...item,
                        workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                        workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                      }
                    })
                  }));*/
                  store.dispatch({
                    type: 'addTestDataArchive',
                    testData: this.props.worksArchiveTestData.map((item, idx) => {
                      const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                      const workRegInvObj = this.props.workRegInvOptions ? this.props.workRegInvOptions[idx * 3] : null;
                      return {
                        ...item,
                        workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                        workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                        workRegInv: !!workRegInvObj ? workRegInvObj.name[this.lng] || workRegInvObj.label || '' : '',
                        workRegInvObj: !!workRegInvObj ? {value: workRegInvObj.id, label: workRegInvObj.name[this.lng]} : {}
                      }
                    })
                  });
                  if(!third.every(data => !!data.data)) Promise.all([this.props.getPropVal(WORK_REG_FUND), this.props.getPropVal(WORK_REG_INV)])
                    .then(() => {
                      /*this.setState(prevState => ({
                        data: prevState.data.map((item, idx) => {
                          const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                          return {
                            ...item,
                            workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                            workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                          }
                        })
                      }));*/
                      store.dispatch({
                        type: 'addTestDataArchive',
                        testData: this.props.worksArchiveTestData.map((item, idx) => {
                          const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
                          const workRegInvObj = this.props.workRegInvOptions ? this.props.workRegInvOptions[idx * 3] : null;
                          return {
                            ...item,
                            workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
                            workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
                            workRegInv: !!workRegInvObj ? workRegInvObj.name[this.lng] || workRegInvObj.label || '' : '',
                            workRegInvObj: !!workRegInvObj ? {value: workRegInvObj.id, label: workRegInvObj.name[this.lng]} : {}
                          }
                        })
                      });
                    })
                })
            });
        });
    }
  }
  stopPropagation = e => {
    e.stopPropagation();
  };

  loadOptions = c => {
    return () => {
      if(!this.props[c + 'Options']) {
        this.setState({[c+'Loading']: true});
        this.props.getPropVal(c)
          .then(() => this.setState({[c+'Loading']: false}))
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    if(!isEmpty(nextProps.works) && !isEmpty(nextProps.tofiConstants) && this.props.works !== nextProps.works) {
      const { doForWorks, dpForWorks } = nextProps.tofiConstants;
      store.dispatch({
        type: 'addTestDataArchive',
        testData: parseCube_new(
          nextProps.works['cube'],
          [],
          'dp',
          'do',
          nextProps.works[`do_${doForWorks.id}`],
          nextProps.works[`dp_${dpForWorks.id}`],
          `do_${doForWorks.id}`,
          `dp_${dpForWorks.id}`).map(this.renderTableData)
      })
      /*this.setState(
        {
          loading: false,
          data: parseCube_new(
            nextProps.works['cube'],
            [],
            'dp',
            'do',
            nextProps.works[`do_${doForWorks.id}`],
            nextProps.works[`dp_${dpForWorks.id}`],
            `do_${doForWorks.id}`,
            `dp_${dpForWorks.id}`).map(this.renderTableData)
        }
      );*/
    } else {
      this.setState({ loading: false });
    }
  }

  addNew = values => {
    console.log(values)
    let newData = this.props.worksArchiveTestData.concat();
    const target = newData.find(el => el.key === values.key);
    if(target) {
      target.workType = !!values.workType ? values.workType.label || '' : '';
      target.workTypeObj = !!values.workType ? values.workType || '' : '';
      target.workRegFund = !!values.workRegFund ? values.workRegFund.label || '' : '';
      target.workRegFundObj = !!values.workRegFund ? values.workRegFund || '' : '';
      target.workRegInv = !!values.workRegInv ? values.workRegInv.label || '' : '';
      target.workRegInvObj = !!values.workRegInv ? values.workRegInv || '' : '';
      target.workPlannedStartDate = !!values.workPlannedStartDate ? values.workPlannedStartDate.format('DD-MM-YYYY') || '' : '';
      target.workPlannedStartDateObj = !!values.workPlannedStartDate ? values.workPlannedStartDate : null;
      target.workPlannedEndDate = !!values.workPlannedEndDate ? values.workPlannedEndDate.format('DD-MM-YYYY') || '' : '';
      target.workPlannedEndDateObj = !!values.workPlannedEndDate ? values.workPlannedEndDate : null;
      target.workAssignedTo = !!values.workAssignedTo ? values.workAssignedTo : '';
      target.workPriority = !!values.workPriority ? values.workPriority : '';
      target.workStatusObj = !!values.workStatus ? values.workStatus : '';
      target.workDate = !!values.workDate ? values.workDate.format('DD-MM-YYYY') || '' : '';
      target.workAuthor = !!values.workAuthor ? values.workAuthor || '' : '';
    } else {
      newData = [
        ...newData,
        {
          key: uuid(),
          // numb: this.state.data.length + 1,
          numb: this.props.worksArchiveTestData.length + 1,
          workType: !!values.workType ? values.workType.label || '' : '',
          workTypeObj: !!values.workType ? values.workType || '' : '',
          workRegFund: !!values.workRegFund ? values.workRegFund.label || '' : '',
          workRegFundObj: !!values.workRegFund ? values.workRegFund || '' : '',
          workRegInv: !!values.workRegInv ? values.workRegInv.label || '' : '',
          workRegInvObj: !!values.workRegInv ? values.workRegInv || '' : '',
          workPlannedStartDate: !!values.workPlannedStartDate ? values.workPlannedStartDate.format('DD-MM-YYYY') || '' : '',
          workPlannedStartDateObj: !!values.workPlannedStartDate ? values.workPlannedStartDate : null,
          workPlannedEndDate: !!values.workPlannedEndDate ? values.workPlannedEndDate.format('DD-MM-YYYY') || '' : '',
          workPlannedEndDateObj: !!values.workPlannedEndDate ? values.workPlannedEndDate : null,
          workAssignedTo: !!values.workAssignedTo ? values.workAssignedTo : '',
          workPriority: !!values.workPriority ? values.workPriority : '',
          workStatusObj: !!values.workStatus ? values.workStatus : '',
          workDate: !!values.workDate ? values.workDate.format('DD-MM-YYYY') || '' : '',
          workAuthor: !!values.workAuthor ? values.workAuthor || '' : ''
        }
      ]
    }

    store.dispatch({
      type: 'addTestDataArchive',
      testData: newData
    });
    // this.setState({data: newData})
  };

  onInputChange = e => {
    this.setState({
      search: {
        ...this.state.search,
        [e.target.name]: e.target.value
      }
    })
  };
  emitEmpty = e => {
    this.setState({search: {
      ...this.state.search,
      [e.target.dataset.name]: ''
    }})
  };

  addSpecialDate = (key, name) => {
    return e => {
      e.stopPropagation();
      // const newData = this.state.data.slice();
      const newData = this.props.worksArchiveTestData.slice();
      const target = newData.find(el => el.key === key);

      if(target) {
        target[name] = moment().format('DD-MM-YYYY');
        if('146'.includes(target.workTypeObj.value) && name === 'workActualStartDate') {
          this.props.history.push(`/archiveFund/works/checking/${target.workRegFund}`)
        }
      }

      // this.setState({ data: newData });
      store.dispatch({
        type: 'addTestDataArchive',
        testData: newData
      })
    };
  };

  renderTableData = (item, idx) => {
    const { workPlannedEndDate, workStatus, workAuthor,
      workPriority, workDate, workAssignedTo, workPlannedStartDate, workActualStartDate, workActualEndDate, acceptanceDate } = this.props.tofiConstants;
    const workPlannedEndDateObj = item.props.find(element => element.prop == workPlannedEndDate.id),
      workStatusObj = item.props.find(element => element.prop == workStatus.id),
      workAuthorObj = item.props.find(element => element.prop == workAuthor.id),
      workPriorityObj = item.props.find(element => element.prop == workPriority.id),
      workDateObj = item.props.find(element => element.prop == workDate.id),
      workAssignedToObj = item.props.find(element => element.prop == workAssignedTo.id),
      workPlannedStartDateObj = item.props.find(element => element.prop == workPlannedStartDate.id),
      workActualStartDateObj = item.props.find(element => element.prop == workActualStartDate.id),
      workActualEndDateObj = item.props.find(element => element.prop == workActualEndDate.id),
      acceptanceDateObj = item.props.find(element => element.prop == acceptanceDate.id);

    const workRegFundObj = this.props.workRegFundOptions ? this.props.workRegFundOptions[idx * 3] : null;
    const workRegInvObj = this.props.workRegInvOptions ? this.props.workRegInvOptions[idx * 3] : null;
    return {
      key: item.id,
      numb: idx + 1,
      // workListName: !!item.name ? item.name[this.lng] || '' : '',
      workType: WORK_TYPE_ARCHIVE_FUND[idx % WORK_TYPE_ARCHIVE_FUND.length].label,
      workTypeObj: WORK_TYPE_ARCHIVE_FUND[idx % WORK_TYPE_ARCHIVE_FUND.length],
      workPlannedStartDate: !!workPlannedStartDateObj ? workPlannedStartDateObj.value || '' : '',
      workPlannedStartDateObj: !!workPlannedStartDateObj && workPlannedStartDateObj.value ? moment(workPlannedStartDateObj.value, 'DD-MM-YYYY') : null,
      workPlannedEndDate: !!workPlannedEndDateObj ? workPlannedEndDateObj.value || '' : '',
      workPlannedEndDateObj: !!workPlannedEndDateObj && workPlannedEndDateObj.value ? moment(workPlannedEndDateObj.value, 'DD-MM-YYYY') : null,
      workStatus: !!workStatusObj ? workStatusObj.value || '' : '',
      workStatusObj: !!workStatusObj ? workStatusObj.refId || '' : '',
      workListNameObj: item.name,
      workPriority: !!workPriorityObj ? workPriorityObj.refId || '' : '',
      workRegFund: !!workRegFundObj ? workRegFundObj.name[this.lng] || workRegFundObj.label || '' : '',
      workRegFundObj: !!workRegFundObj ? {value: workRegFundObj.id, label: workRegFundObj.name[this.lng]} : {},
      workRegInv: !!workRegInvObj ? workRegInvObj.name[this.lng] || workRegInvObj.label || '' : '',
      workRegInvObj: !!workRegInvObj ? {value: workRegInvObj.id, label: workRegInvObj.name[this.lng]} : {},
      workAuthor: !!workAuthorObj ? workAuthorObj.value || '' : '',
      workDate: !!workDateObj && workDateObj.value ? moment(workDateObj.value, 'DD-MM-YYYY') : null,
      workAssignedTo: !!workAssignedToObj ? workAssignedToObj.cube.idRef || '' : '',
      workActualStartDate: !!workActualStartDateObj ? workActualStartDateObj.value || '' : '',
      workActualStartDateObj: !!workActualStartDateObj && workActualStartDateObj.value ? moment(workActualStartDateObj.value, 'DD-MM-YYYY') : null,
      workActualEndDate: !!workActualEndDateObj ? workActualEndDateObj.value || '' : '',
      workActualEndDateObj: !!workActualEndDateObj && workActualEndDateObj.value ? moment(workActualEndDateObj.value, 'DD-MM-YYYY') : null,
      acceptanceDate: !!acceptanceDateObj ? acceptanceDateObj.value || '' : '',
      acceptanceDateObj: !!acceptanceDateObj && acceptanceDateObj.value ? moment(acceptanceDateObj.value, 'DD-MM-YYYY') : null
    }
  };

  menu = (
    <Menu>
      <Menu.Item key="first">{this.props.t('REPORT_1')}</Menu.Item>
      <Menu.Item key="second">{this.props.t('REPORT_2')}</Menu.Item>
    </Menu>
  );

  render() {
    const { search, loading, performer, status, priority, workPriorityLoading, workStatusLoading, workAssignedToLoading } = this.state;
    const { t, tofiConstants, worksArchiveTestData } = this.props;
    if(!tofiConstants) return null;

    this.lng = localStorage.getItem('i18nextLng');
    const { workPlannedStartDate, workPlannedEndDate, workActualStartDate, workActualEndDate, acceptanceDate, workType, workRegFund, workRegInv } = tofiConstants;

    this.filteredData = worksArchiveTestData.filter(item => {
      return (
        // item.numb === Number(search) ||
        item.workType.toLowerCase().includes(search.workType.toLowerCase()) &&
        item.workRegFund.toLowerCase().includes(search.workRegFund.toLowerCase()) &&
        item.workRegInv.toLowerCase().includes(search.workRegInv.toLowerCase()) &&
        ( priority.length === 0  || priority.some(p => p.value == item.workPriority) ) &&
        ( status.length === 0  || status.some(p => p.value == item.workStatusObj) ) &&
        ( performer.length === 0  || performer.some(p => p.value == item.workAssignedTo) ) &&
        ( !search.workPlannedStartDate.dbeg || moment(item.workPlannedStartDate, 'DD-MM-YYYY').isSameOrAfter(search.workPlannedStartDate.dbeg, 'day') ) &&
        ( !search.workPlannedStartDate.dend || moment(item.workPlannedStartDate, 'DD-MM-YYYY').isSameOrBefore(search.workPlannedStartDate.dend, 'day') ) &&
        ( !search.workPlannedEndDate.dbeg || moment(item.workPlannedEndDate, 'DD-MM-YYYY').isSameOrAfter(search.workPlannedEndDate.dbeg, 'day') ) &&
        ( !search.workPlannedEndDate.dend || moment(item.workPlannedEndDate, 'DD-MM-YYYY').isSameOrBefore(search.workPlannedEndDate.dend, 'day') ) &&
        ( !search.workActualStartDate.dbeg || moment(item.workActualStartDate, 'DD-MM-YYYY').isSameOrAfter(search.workActualStartDate.dbeg, 'day') ) &&
        ( !search.workActualStartDate.dend || moment(item.workActualStartDate, 'DD-MM-YYYY').isSameOrBefore(search.workActualStartDate.dend, 'day') ) &&
        ( !search.workActualEndDate.dbeg || moment(item.workActualEndDate, 'DD-MM-YYYY').isSameOrAfter(search.workActualEndDate.dbeg, 'day') ) &&
        ( !search.workActualEndDate.dend || moment(item.workActualEndDate, 'DD-MM-YYYY').isSameOrBefore(search.workActualEndDate.dend, 'day') ) &&
        ( !search.acceptanceDate.dbeg || moment(item.acceptanceDate, 'DD-MM-YYYY').isSameOrAfter(search.acceptanceDate.dbeg, 'day') ) &&
        ( !search.acceptanceDate.dend || moment(item.acceptanceDate, 'DD-MM-YYYY').isSameOrBefore(search.acceptanceDate.dend, 'day') )
        // item.workPlannedEndDate.toLowerCase().includes(search.toLowerCase()) ||
        // item.workActualStartDate.toLowerCase().includes(search.toLowerCase())
      );
    });

    return (
      <div className="Works">
        <div className="title">
          <h2>Работы по учету и хранению</h2>
        </div>
        <div className="Works__heading">
          <div className="table-header">
            <Button onClick={this.openCard}>{this.props.t('ADD')}</Button>
            {/*<RadioGroup defaultValue={1} onChange={this.onRadioChange}>
              <Radio value={1}>Только мои работы</Radio>
              <Radio value={2}>Все работы</Radio>
            </RadioGroup>*/}
            <div className="label-select">
              <Select
                name="priority"
                multi
                searchable={false}
                value={priority}
                onChange={this.onPriorityChange}
                isLoading={workPriorityLoading}
                options={this.props.workPriorityOptions ? this.props.workPriorityOptions.map(option => ({value: option.id, label: option.name[this.lng]})) : []}
                placeholder={t('PRIORITY')}
                onOpen={this.loadOptions(WORK_PRIORITY)}
              />
            </div>
            <div className="label-select">
              <Select
                name="status"
                multi
                searchable={false}
                value={status}
                onChange={this.onStatusChange}
                isLoading={workStatusLoading}
                options={this.props.workStatusOptions ? this.props.workStatusOptions.map(option => ({value: option.id, label: option.name[this.lng]})) : []}
                placeholder={t('STATUS')}
                onOpen={this.loadOptions(WORK_STATUS)}
              />
            </div>
            {/*<div className="label-select">
              <p>{t('SOURCING')}</p>
              <SelectVirt
                name="sourcing"
                async
                optionHeight={45}
                searchable
                className="long-selected-menu"
                value={sourcing}
                onChange={this.onSourcingChange}
                loadOptions={this.getWorkSourceOptions}
                menuStyle={{width: 300}}
              />
            </div>
            <div className="label-select">
              <p>{t('FORM')}</p>
              <Select
                name="form"
                searchable={false}
                menuStyle={{width: 200}}
                menuContainerStyle={{width: 202}}
                value={form}
                onChange={this.onFormChange}
                options={[
                  {
                    label: 'one',
                    value: '1'
                  },
                  {
                    label: 'two',
                    value: '2'
                  }
                ]}
              />
            </div>*/}
            <div className="label-select">
              <SelectVirt
                name="performer"
                multi
                searchable
                className="long-selected-menu"
                // async
                isLoading={workAssignedToLoading}
                onOpen={this.loadOptions(WORK_ASSIGNED_TO)}
                optionHeight={45}
                value={performer}
                onChange={this.onPerformerChange}
                options={this.props.workAssignedToOptions ? this.props.workAssignedToOptions.map(option => ({value: option.id, label: option.name[this.lng]})) : []}
                placeholder={t('PERFORMER')}
              />
            </div>
            <div className="label-select">
              <Dropdown overlay={this.menu} trigger={['click']}>
                <Button style={{ marginLeft: 8 }}>
                  {this.props.t('REPORT')} <Icon type="printer" />
                </Button>
              </Dropdown>
            </div>
          </div>
        </div>
        <div className="Works__body">
          <AntTable
            loading={loading}
            columns={[
              {
                key: 'numb',
                title: '№',
                dataIndex: 'numb',
                width: '5%'
              },
              {
                key: 'workType',
                title: workType.name[this.lng],
                dataIndex: 'workType',
                width: '10%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <Input
                      name="workType"
                      suffix={search.workType ? <Icon type="close-circle" data-name="workType" onClick={this.emitEmpty} /> : null}
                      ref={ele => this.workType = ele}
                      placeholder="Поиск"
                      value={search.workType}
                      onChange={this.onInputChange}
                    />
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: search.workType ? '#ff9800' : '#aaa' }} />,
                onFilterDropdownVisibleChange: (visible) => {
                  this.setState({
                    filterDropdownVisible: visible,
                  }, () => this.workType.focus());
                },
              },
              {
                key: 'workRegFund',
                title: workRegFund.name[this.lng],
                dataIndex: 'workRegFund',
                width: '10%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <Input
                      name="workRegFund"
                      suffix={search.workRegFund ? <Icon type="close-circle" data-name="workRegFund" onClick={this.emitEmpty} /> : null}
                      ref={ele => this.workRegFund = ele}
                      placeholder="Поиск"
                      value={search.workRegFund}
                      onChange={this.onInputChange}
                    />
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: search.workRegFund ? '#ff9800' : '#aaa' }} />,
                onFilterDropdownVisibleChange: (visible) => {
                  this.setState({
                    filterDropdownVisible: visible,
                  }, () => this.workRegFund.focus());
                }
              },
              {
                key: 'workRegInv',
                title: workRegInv.name[this.lng],
                dataIndex: 'workRegInv',
                width: '10%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <Input
                      name="workRegInv"
                      suffix={search.workRegInv ? <Icon type="close-circle" data-name="workRegInv" onClick={this.emitEmpty} /> : null}
                      ref={ele => this.workRegInv = ele}
                      placeholder="Поиск"
                      value={search.workRegInv}
                      onChange={this.onInputChange}
                    />
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: search.workRegInv ? '#ff9800' : '#aaa' }} />,
                onFilterDropdownVisibleChange: (visible) => {
                  this.setState({
                    filterDropdownVisible: visible,
                  }, () => this.workRegInv.focus());
                }
              },
              {
                key: 'workPlannedStartDate',
                title: workPlannedStartDate.name[this.lng],
                dataIndex: 'workPlannedStartDate',
                width: '10%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <div className="flex">
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workPlannedStartDate.dbeg}
                        style={{marginRight: '16px'}}
                        showToday={false}
                        onChange={this.onDateChange('workPlannedStartDate', 'dbeg')}
                      />
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workPlannedStartDate.dend}
                        showToday={false}
                        onChange={this.onDateChange('workPlannedStartDate', 'dend')}
                      />
                    </div>
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: (search.workPlannedStartDate.dbeg || search.workPlannedStartDate.dend) ? '#ff9800' : '#aaa' }} />
              },
              {
                key: 'workPlannedEndDate',
                title: workPlannedEndDate.name[this.lng],
                dataIndex: 'workPlannedEndDate',
                width: '12%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <div className="flex">
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workPlannedEndDate.dbeg}
                        style={{marginRight: '16px'}}
                        showToday={false}
                        onChange={this.onDateChange('workPlannedEndDate', 'dbeg')}
                      />
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workPlannedEndDate.dend}
                        showToday={false}
                        onChange={this.onDateChange('workPlannedEndDate', 'dend')}
                      />
                    </div>
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: (search.workPlannedEndDate.dbeg || search.workPlannedEndDate.dend) ? '#ff9800' : '#aaa' }} />
              },
              {
                key: 'workActualStartDate',
                title: workActualStartDate.name[this.lng],
                dataIndex: 'workActualStartDate',
                width: '12%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <div className="flex">
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workActualStartDate.dbeg}
                        style={{marginRight: '16px'}}
                        showToday={false}
                        onChange={this.onDateChange('workActualStartDate', 'dbeg')}
                      />
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workActualStartDate.dend}
                        showToday={false}
                        onChange={this.onDateChange('workActualStartDate', 'dend')}
                      />
                    </div>
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: (search.workActualStartDate.dbeg || search.workActualStartDate.dend) ? '#ff9800' : '#aaa' }} />
              },
              {
                key: 'workActualEndDate',
                title: workActualEndDate.name[this.lng],
                dataIndex: 'workActualEndDate',
                width: '12%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <div className="flex">
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workActualEndDate.dbeg}
                        style={{marginRight: '16px'}}
                        showToday={false}
                        onChange={this.onDateChange('workActualEndDate', 'dbeg')}
                      />
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.workActualEndDate.dend}
                        showToday={false}
                        onChange={this.onDateChange('workActualEndDate', 'dend')}
                      />
                    </div>
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: (search.workActualEndDate.dbeg || search.workActualEndDate.dend) ? '#ff9800' : '#aaa' }} />
              },
              {
                key: 'acceptanceDate',
                title: acceptanceDate.name[this.lng],
                dataIndex: 'acceptanceDate',
                width: '10%',
                filterDropdown: (
                  <div className="custom-filter-dropdown">
                    <div className="flex">
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.acceptanceDate.dbeg}
                        style={{marginRight: '16px'}}
                        showToday={false}
                        onChange={this.onDateChange('acceptanceDate', 'dbeg')}
                      />
                      <DatePicker
                        format="DD-MM-YYYY"
                        value={this.state.search.acceptanceDate.dend}
                        showToday={false}
                        onChange={this.onDateChange('acceptanceDate', 'dend')}
                      />
                    </div>
                  </div>
                ),
                filterIcon: <Icon type="filter" style={{ color: (search.acceptanceDate.dbeg || search.acceptanceDate.dend) ? '#ff9800' : '#aaa' }} />
              },
              {
                key: 'action',
                title: '',
                dataIndex: 'action',
                width: '8%',
                render: (text, record) => {
                  return (
                    <div className="editable-row-operations">
                      <span>
                        <Popconfirm title={this.props.t('CONFIRM_REMOVE')} onConfirm={() => console.log(record.key)}>
                          <Button title="Удалить" icon="delete" onClick={this.stopPropagation} disabled={!!record.workActualStartDate} className='green-btn yellow-bg'/>
                        </Popconfirm>
                        <Button title="Начать" icon="play-circle" onClick={this.addSpecialDate(record.key, 'workActualStartDate')} disabled={!!record.workActualStartDate &&  !'146'.includes(record.workTypeObj.value)} className='green-btn'/>
                        <Button title="Завершить" icon="poweroff" disabled={!!record.workActualEndDate} onClick={this.addSpecialDate(record.key, 'workActualEndDate')} className='green-btn'/>
                        <Button title="Принять" icon="check-circle" className='green-btn' disabled={!!record.acceptanceDate} onClick={this.addSpecialDate(record.key, 'acceptanceDate')} />
                      </span>
                    </div>
                  );
                },
              }
            ]}
            dataSource={this.filteredData}
            changeSelectedRow={this.changeSelectedRow}
            openedBy="Works"
            // size="small"
          />
          <CSSTransition
            in={this.state.openCard}
            timeout={300}
            classNames="card"
            unmountOnExit
          >
            <SiderCard t={t} tofiConstants={tofiConstants} initialValues={this.state.initialValues} addNew={this.addNew} onOpen={this.loadOptions}
                       closer={<Button type='danger' onClick={this.closeCard} shape="circle" icon="close"/>}
                       optionsData={{workRegFundOptions: this.props.workRegFundOptions, workRegInvOptions: this.props.workRegInvOptions, workPriorityOptions: this.props.workPriorityOptions,
                         workStatusOptions:this.props.workStatusOptions, workAssignedToOptions: this.props.workAssignedToOptions}}
            />
          </CSSTransition>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    tofiConstants: state.generalData.tofiConstants,
    works: state.cubes[CUBE_FOR_WORKS],
    worksArchiveTestData: state.cubes.worksArchiveTestData,
    workRegFundOptions: state.generalData[WORK_REG_FUND],
    workRegInvOptions: state.generalData[WORK_REG_INV],
    workPriorityOptions: state.generalData[WORK_PRIORITY],
    workStatusOptions: state.generalData[WORK_STATUS],
    workAssignedToOptions: state.generalData[WORK_ASSIGNED_TO]
  }
}

export default connect(mapStateToProps, {getCube, getPropVal})(ArchiveFundWorks);