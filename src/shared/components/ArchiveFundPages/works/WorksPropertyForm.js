import React, { Component } from 'react';
import {Button, Form} from 'antd';
import { Field, reduxForm } from 'redux-form';
import {
  renderDatePicker, renderInput, renderSelect, renderSelectVirt
} from '../../../utils/form_components';
import { WORK_TYPE_ARCHIVE_FUND } from '../../../constants/constants'

class WorksPropertyForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      lang: {
        workListName: localStorage.getItem('i18nextLng'),
      }
    };
  }

  changeLang = e => {
    this.setState({lang: {...this.state.lang, [e.target.name]: e.target.value}});
  };

  // workName = {...this.props.initialValues.workListName} || {kz: '', ru: '', en: ''};

  onSubmit = values => {
    this.props.addNew(values);
  };

  render() {
    if(!this.props.tofiConstants) return null;

    const lng = localStorage.getItem('i18nextLng');
    const { t, handleSubmit, reset, dirty, error, submitting, tofiConstants: {workType, workPlannedEndDate,
      workPriority, workStatus, workRegFund, workRegInv, workAuthor, workDate, workAssignedTo,
      workPlannedStartDate}, optionsData } = this.props;
    // const { lang } = this.state;

    return (
      <Form className="antForm-spaceBetween" onSubmit={handleSubmit(this.onSubmit)} style={dirty ? {paddingBottom: '43px'} : {}}>
        {/*{workListName && <Field
          name="workListName"
          component={ renderInputLang }
          format={value => (!!value ? value[lang.workListName] : '')}
          parse={value => { this.workName[lang.workListName] = value; return {...this.workName} }}
          label={workListName.name[lng]}
          formItemClass="with-lang"
          changeLang={this.changeLang}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          // validate={requiredLng}
          // colon={true}
        />}*/}
        {/*{workType && <Field
          name="workType"
          component={ renderAsyncSelect }
          searchable={false}
          label={workType.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          loadOptions={this.getWorkTypeOptions}
          // validate={required}
          // colon={true}
        />}*/}
        <Field
          name="workType"
          component={ renderSelect }
          searchable={false}
          label={workType.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          data={WORK_TYPE_ARCHIVE_FUND}
        />
        {workRegFund && <Field
          name="workRegFund"
          component={ renderSelectVirt }
          label={workRegFund.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          options={optionsData.workRegFundOptions ? optionsData.workRegFundOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
          // validate={requiredLabel}
          // colon={true}
        />}
        {workRegInv && <Field
          name="workRegInv"
          component={ renderSelectVirt }
          label={workRegInv.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          options={optionsData.workRegInvOptions ? optionsData.workRegInvOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
          // validate={requiredLabel}
          // colon={true}
        />}
        {workPlannedStartDate && <Field
          name="workPlannedStartDate"
          component={renderDatePicker}
          format={null}
          label={workPlannedStartDate.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
        />}
        {workPlannedEndDate && <Field
          name="workPlannedEndDate"
          component={ renderDatePicker }
          format={null}
          searchable={false}
          label={workPlannedEndDate.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
        />}
        {workAssignedTo && <Field
          name="workAssignedTo"
          component={renderSelect}
          label={workAssignedTo.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
          // validate={requiredDate}
          // colon={true}
          data={optionsData.workAssignedToOptions ? optionsData.workAssignedToOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
          onOpen={this.props.onOpen('workAssignedTo')}
        />}
        {workPriority && <Field
          name="workPriority"
          component={ renderSelect }
          searchable={false}
          label={workPriority.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          data={optionsData.workPriorityOptions ? optionsData.workPriorityOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
          onOpen={this.props.onOpen('workPriority')}
          // validate={requiredLabel}
          // colon={true}
        />}
        {/*{workActualStartDate && <Field
          name="workActualStartDate"
          component={renderDatePicker}
          format={null}
          label={workActualStartDate.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
        />}
        {workActualEndDate && <Field
          name="workActualEndDate"
          component={renderDatePicker}
          format={null}
          label={workActualEndDate.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
        />}
        {acceptanceDate && <Field
          name="acceptanceDate"
          component={renderDatePicker}
          format={null}
          label={acceptanceDate.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
        />}*/}
        {workStatus && <Field
          name="workStatus"
          disabled
          component={ renderSelect }
          searchable={false}
          label={workStatus.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          onOpen={this.props.onOpen('workStatus')}
          // validate={requiredLabel}
          // colon={true}
          data={optionsData.workStatusOptions ? optionsData.workStatusOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
        />}
        {workDate && <Field
          name="workDate"
          component={renderDatePicker}
          disabled
          format={null}
          label={workDate.name[lng]}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
          // defaultValue={moment()}
          // validate={requiredDate}
          // colon={true}
        />}
        {workAuthor && <Field
          name="workAuthor"
          component={ renderInput }
          readOnly
          placeholder={t('USER_FIO_PLACEHOLDER')}
          label={workAuthor.name[lng]}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
        />}
        {dirty && <Form.Item className="ant-form-btns">
          <Button className="signup-form__btn" type="primary" htmlType="submit" disabled={submitting}>
            {submitting ? t('LOADING...') : t('SAVE') }
          </Button>
          <Button className="signup-form__btn" type="danger" htmlType="button" disabled={submitting} style={{marginLeft: '10px'}} onClick={reset}>
            {submitting ? t('LOADING...') : t('CANCEL') }
          </Button>
          {error && <span className="message-error"><i className="icon-error" />{error}</span>}
        </Form.Item>}
      </Form>
    )
  }
}

export default reduxForm({ form: 'WorksPropertyForm', enableReinitialize: true })(WorksPropertyForm);
