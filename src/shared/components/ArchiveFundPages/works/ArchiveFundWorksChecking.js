import React from 'react';
import {Breadcrumb, Checkbox, Table} from 'antd';
import {Link} from 'react-router-dom';

class RenderCheckbox extends React.Component {
  state = {
    checked: false
  };

  componentDidMount() {
    this.setState({checked: this.props.checked})
  }

  onChange = e => {
    this.setState({ checked: e.target.checked });
  };

  render() {
    return <Checkbox checked={this.state.checked} onChange={this.onChange}/>
  }
}

class ArchiveFundWorksChecking extends React.PureComponent {

  state = {
    data: [
      {
        key: '1',
        numb: '1',
        cases: 'Дело 1',
        availability: true,
        valuable: false,
        requiresFiling: false,
        requiresRestoration: false,
        requiresDisinfection: true,
        requiresRestorationOfFadingText: false,
        requiresDisinfestation: false,
        forProcessing: false,
        forDigitizing: true,
        forRR: true,
        requiresCartoning: false,
        forDocsDescribing: true,
        forTemporaryUse: true,
        forCreatingInsurance: false
      },
      {
        key: '2',
        numb: '2',
        cases: 'Дело 2',
        availability: false,
        valuable: false,
        requiresFiling: true,
        requiresRestoration: false,
        requiresDisinfection: true,
        requiresRestorationOfFadingText: true,
        requiresDisinfestation: true,
        forProcessing: false,
        forDigitizing: true,
        forRR: true,
        requiresCartoning: true,
        forDocsDescribing: true,
        forTemporaryUse: true,
        forCreatingInsurance: false
      },
      {
        key: '3',
        numb: '3',
        cases: 'Дело 3',
        availability: true,
        valuable: false,
        requiresFiling: false,
        requiresRestoration: true,
        requiresDisinfection: true,
        requiresRestorationOfFadingText: true,
        requiresDisinfestation: true,
        forProcessing: false,
        forDigitizing: false,
        forRR: false,
        requiresCartoning: false,
        forDocsDescribing: false,
        forTemporaryUse: true,
        forCreatingInsurance: false
      },
      {
        key: '4',
        numb: '4',
        cases: 'Дело 4',
        availability: false,
        valuable: false,
        requiresFiling: false,
        requiresRestoration: true,
        requiresDisinfection: true,
        requiresRestorationOfFadingText: true,
        requiresDisinfestation: true,
        forProcessing: false,
        forDigitizing: false,
        forRR: false,
        requiresCartoning: false,
        forDocsDescribing: false,
        forTemporaryUse: true,
        forCreatingInsurance: false
      }
    ],
  };

  renderTableHeader = () => {
    return (
      <div className="table-header">
        <Breadcrumb>
          <Breadcrumb.Item><Link to="/archiveFund/works">Работы по учету и хранению</Link></Breadcrumb.Item>
          <Breadcrumb.Item><b>{this.props.match.params.fund}</b></Breadcrumb.Item>
        </Breadcrumb>
      </div>
    )
  };

  render() {
    return (
      <div className="WorksChecking">
        <Table
          columns={[
            {
              key: 'numb',
              title: 'Номер',
              dataIndex: 'numb',
              width: '5%',
            },
            {
              key: 'cases',
              title: 'дело',
              dataIndex: 'cases',
              width: '11%',
            },
            {
              key: 'availability',
              title: 'наличие',
              dataIndex: 'availability',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'valuable',
              title: 'Особо ценное',
              dataIndex: 'valuable',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresFiling',
              title: 'Требует подшивки',
              dataIndex: 'requiresFiling',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresRestoration',
              title: 'Требует реставрации',
              dataIndex: 'requiresRestoration',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresDisinfection',
              title: 'Требует дезинфекции',
              dataIndex: 'requiresDisinfection',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresRestorationOfFadingText',
              title: 'Требует восстановления затухающего текста',
              dataIndex: 'requiresRestorationOfFadingText',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresDisinfestation',
              title: 'Требует дезинсекции',
              dataIndex: 'requiresDisinfestation',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forProcessing',
              title: 'Для консервационно-профилактической обработки',
              dataIndex: 'forProcessing',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forDigitizing',
              title: 'Для оцифровки',
              dataIndex: 'forDigitizing',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forRR',
              title: 'Для читального зала',
              dataIndex: 'forRR',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'requiresCartoning',
              title: 'Требует картонирования',
              dataIndex: 'requiresCartoning',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forDocsDescribing',
              title: 'Для описания документов',
              dataIndex: 'forDocsDescribing',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forTemporaryUse',
              title: 'Во временное пользование сторонним',
              dataIndex: 'forTemporaryUse',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
            {
              key: 'forCreatingInsurance',
              title: 'Для создания страхового фонда',
              dataIndex: 'forCreatingInsurance',
              width: '6%',
              render: (text, record) => (
                <RenderCheckbox checked={text}/>
              )
            },
          ]}
          bordered
          size="small"
          title={this.renderTableHeader}
          dataSource={this.state.data}
          pagination={false}
          scroll={{x: 1500}}
        />
      </div>
    )
  }
}

export default ArchiveFundWorksChecking;