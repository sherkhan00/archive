import React, { Component } from 'react';
import { Form } from 'antd';
import { Field, reduxForm } from 'redux-form';
import {required, requiredDate, requiredLabel, requiredLng} from '../../utils/form_validations';
import {
  renderAsyncSelect, renderDatePicker, renderInput, renderInputLang, renderRadioGroup
} from '../../utils/form_components';
import {getPropValByConst, getAccessLevels} from '../../actions/actions';
import {four_digits} from '../../utils/form_normalizing';

class AddNewFundForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      clsFundConst: 'clsFundOrg',
      fundCategory: '',
      accessLevel: '',
      fundName: {en: 'en', ru: ''},
      lng: {
        fundName: localStorage.getItem('i18nextLng'),
        fundShortName: localStorage.getItem('i18nextLng')
      }
    };
  }

  getCategories = () => {
    return getPropValByConst('fundCategory')
      .then(json=>({
        options: json.data.map(item => (
          {value: item.id, label: item.name[localStorage.getItem('i18nextLng')]})
        )})
      )
  };

  getFundMakers = () => {
    return getPropValByConst('fundMaker')
      .then(json=>({
        options: json.data.map(item => (
          {value: item.id, label: item.name[localStorage.getItem('i18nextLng')]})
        )})
      )
  };

  getAccessLevels = () => {
    return getAccessLevels()
      .then(json=>({
        options: json.map(item => (
          {value: item.id, label: item.name[localStorage.getItem('i18nextLng')]})
        )})
      )
  };

  changeLang = e => {
    this.setState({lng: {...this.state.lng, [e.target.name]: e.target.value}});
  };

  fundName = {kz: '', ru: '', en: ''};
  fundShortName = {kz: '', ru: '', en: ''};

  render() {
    const { t } = this.props;
    const { lng: {fundName, fundShortName} } = this.state;

    return (
      <Form className="antForm-spaceBetween">
        <Field
          name="clsFundConst"
          component={ renderRadioGroup }
          data={
            [
              {label: t('ORGANIZATION_FUND'), value:'clsFundOrg'},
              {label: t('PERSONAL_FUND'), value: 'clsFundLP'}
            ]
          }
          onChange={(e, newFalue) => this.setState({ clsFundConst: newFalue })}
          label={t('ADD_NEW_FUND')+':'}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
        />
        <Field
          name="fundName"
          component={ renderInputLang }
          format={value => (!!value ? value[fundName] : '')}
          parse={value => { this.fundName[fundName] = value; return {...this.fundName} }}
          label={t('FUND_NAME')}
          formItemClass="with-lang"
          changeLang={this.changeLang}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={requiredLng}
          colon={true}
        />
        <Field
          name="fundShortName"
          component={ renderInputLang }
          format={value => (!!value ? value[fundShortName] : '')}
          parse={value => { this.fundShortName[fundShortName] = value; return {...this.fundShortName} }}
          formItemClass="with-lang"
          label={t('FUND_SHORT_NAME')}
          changeLang={this.changeLang}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={requiredLng}
          colon={true}
        />
        <Field
          name="fundNumber"
          component={ renderInput }
          label={t('FUND_NUMB')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={required}
          colon={true}
        />
        <Field
          name="fundIndex"
          component={ renderInput }
          type="text"
          placeholder=""
          label={t('FUND_INDEX')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
        />
        <Field
          name="fundMaker"
          component={ renderAsyncSelect }
          loadOptions={this.getFundMakers}
          searchable={false}
          label={t('FUND_MAKER')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={requiredLabel}
          colon={true}
        />
        <Field
          name="dbeg"
          component={ renderInput }
          type="text"
          placeholder=""
          label={t('DBEG')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          normalize={four_digits}
        />
        <Field
          name="dend"
          component={ renderInput }
          type="text"
          placeholder=""
          label={t('DEND')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          normalize={four_digits}
        />
        <Field
          name="fundCategory"
          component={ renderAsyncSelect }
          searchable={false}
          loadOptions={this.getCategories}
          label={t('FUND_CATEGORY')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={requiredLabel}
          colon={true}
        />
        <Field
          name="formationDate"
          component={renderDatePicker}
          format={null}
          label={this.state.clsFundConst === 'clsFundOrg' ? t('FORMATION_DATE') : t('BIRTH_DATE')}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
          validate={requiredDate}
          colon={true}
        />
        <Field
          name="eliminationDate"
          component={renderDatePicker}
          format={null}
          label={this.state.clsFundConst === 'clsFundOrg' ? t('ELIMINATION_DATE') : t('DEATH_DATE')}
          formItemLayout={
            {
              labelCol: {span: 10},
              wrapperCol: {span: 14}
            }
          }
        />
        <Field
          name="accessLevel"
          id="accessLevel"
          component={ renderAsyncSelect }
          searchable={false}
          loadOptions={this.getAccessLevels}
          label={t('ACCESS_LEVEL')}
          formItemLayout={
            {
              labelCol: { span: 10 },
              wrapperCol: { span: 14 }
            }
          }
          validate={requiredLabel}
          colon={true}
        />
      </Form>
    )
  }
}

export default reduxForm({ form: 'AddNewFundForm', initialValues: {clsFundConst: 'clsFundOrg', formationDate: null} })(AddNewFundForm);
