import React from 'react';
import {Button, Table, Form, Tree, Icon, Popconfirm, Input, DatePicker, message} from 'antd';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {CSSTransition} from 'react-transition-group';
import SiderCardLegalEntities from './SiderCardLegalEntities';
import {isEmpty, map} from 'lodash';

import {SYSTEM_LANG_ARRAY} from '../../../constants/constants';
import {
  renderDatePicker, renderFileUpload, renderInput, renderInputLang,
  renderSelect
} from '../../../utils/form_components';
import {connect} from 'react-redux';
import {createObj, getObjList, getPropVal} from '../../../actions/actions';
import {required, requiredLabel} from '../../../utils/form_validations';
import moment from 'moment';
import {getPropMeta} from '../../../utils/cubeParser';

const LegalEntitiesInventoryProps = (
  {tofiConstants, lng, submitting, error, reset,
   t, handleSubmit, createNewObj, loadOptions,
   invTypeOptions, invCaseSystemOptions, invTypeValue}
 ) => {

  const handleFormSubmit = values => {
    const name = {};
    SYSTEM_LANG_ARRAY.forEach(lang => {
      name[lang] = values.name
    });
    const obj = {
      name,
      fullName: name,
      clsConst: 'invList',
    };
    createNewObj({obj, cube: {cubeSConst: 'CubeForAF_Inv', doConst: 'doForInv', dpConst: 'dpForInv'}}, values, {});
  };
  const { invNumber, invType, invCaseSystem, nomen, casesQuantity, invTypePerm, approvalProtocol, agreementProtocol,
    invDeadline, invCreateDate, invApprovalDate1, invApprovalDate2, invAgreementDate, lastChangeDate, invDates } = tofiConstants;

  return (
    <Form className="antForm-spaceBetween" onSubmit={handleSubmit(handleFormSubmit)}>
      {invNumber && <Field
        name='invNumber'
        colon
        component={ renderInput }
        label={invNumber.name[lng]}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
        validate={required}
      />}
      <Field
        name='name'
        component={ renderInput }
        label={t('NAME')}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
        colon
        validate={required}
      />
      {invDates && <Field
        name='invDates'
        component={ renderInput }
        label={invDates.name[lng]}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invType && <Field
        name='invType'
        component={ renderSelect }
        label={invType.name[lng]}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
        onOpen={loadOptions('invType')}
        data={invTypeOptions ? invTypeOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
        colon
        validate={requiredLabel}
      />}
      {invCaseSystem && <Field
        name='invCaseSystem'
        component={ renderSelect }
        label={invCaseSystem.name[lng]}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
        onOpen={loadOptions('invCaseSystem')}
        data={invCaseSystemOptions ? invCaseSystemOptions.map(option => ({value: option.id, label: option.name[lng]})) : []}
      />}
      {nomen && <Field
        name='nomen'
        component={ renderSelect }
        label={ nomen.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
        data={[{value: 'one', label: 'one'}]}
        colon
        validate={requiredLabel}
      />}
      {casesQuantity && <Field
        name='casesQuantity'
        component={ renderInput }
        label={ casesQuantity.name[lng] }
        type="number"
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invDeadline && <Field
        name='invDeadline'
        component={ renderDatePicker }
        format={null}
        label={invDeadline.name[lng]}
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invCreateDate && <Field
        name='invCreateDate'
        component={ renderDatePicker }
        format={null}
        label={ invCreateDate.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invAgreementDate && <Field
        name='invAgreementDate'
        component={ renderDatePicker }
        format={null}
        label={ invAgreementDate.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invApprovalDate1 && <Field
        name='invApprovalDate1'
        component={ renderDatePicker }
        format={null}
        label={ invApprovalDate1.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {invTypeValue && invTypeValue.value == invTypePerm.id && invApprovalDate2 && <Field
        name='invApprovalDate2'
        component={ renderDatePicker }
        format={null}
        label={ invApprovalDate2.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {lastChangeDate && <Field
        name='lastChangeDate'
        component={ renderDatePicker }
        format={null}
        label={ lastChangeDate.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {approvalProtocol && <Field
        name='approvalProtocol'
        component={ renderFileUpload }
        label={ approvalProtocol.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      {agreementProtocol && <Field
        name='agreementProtocol'
        component={ renderFileUpload }
        label={ agreementProtocol.name[lng] }
        formItemLayout={
          {
            labelCol: { span: 12 },
            wrapperCol: { span: 12 }
          }
        }
      />}
      <Form.Item className="ant-form-btns">
        <Button className="signup-form__btn" type="primary" htmlType="submit" disabled={submitting}>
          {submitting ? t('LOADING...') : t('SAVE') }
        </Button>
        <Button className="signup-form__btn" type="danger" htmlType="button" disabled={submitting} style={{marginLeft: '10px'}} onClick={reset}>
          {submitting ? t('LOADING...') : t('CANCEL') }
        </Button>
        {error && <span className="message-error"><i className="icon-error" />{error}</span>}
      </Form.Item>
    </Form>
  );
};

const selector = formValueSelector('LegalEntitiesInventoryPropsForm');
const LegalEntitiesInventoryPropsForm = connect(state => {
  const invTypeValue = selector(state, 'invType');
  return {
    invTypeValue,
  }
})(reduxForm({ form: 'LegalEntitiesInventoryPropsForm', enableReinitialize: true })(LegalEntitiesInventoryProps));

const EditableCell = ({ editable, value, onChange }) => (
  <span className="editable-cell">
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </span>
);
const EditableDatePicker = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <DatePicker style={{ margin: '-5px 0' }} format="DD-MM-YYYY" value={value ? moment(value, 'DD-MM-YYYY') : null} onChange={e => onChange(e)} />
      : value
    }
  </div>
);

const TreeNode = Tree.TreeNode;

class LegalEntitiesInventoriesMain extends React.Component {

  state = {
    data: [],
    loading: false,
    formData: {},
    treeData: [],
    selectedRow: {},
    openCard: false,
    linv: {},
    checkedKeys: [],
  };

  onCheck = (checkedKeys, e) => {
    const lastCheckedKeys = checkedKeys.filter(key => e.checkedNodes.find(o => o.key === key).props.isLeaf);
    this.setState({ checkedKeys: lastCheckedKeys });
  };

  addNew = () => {
    this.setState({
      data: [
        ...this.state.data,
        {
          key: `newData_${this.state.data.length}`,
          parent: this.state.ln.id,
          objClass: 'structuralSubdivisionList',
          editable: true,
          caseOCD: ''
        }
      ]
    })
  };
  addNewChild = type => {
    const newData = this.state.data.slice();
    const key = this.state.selectedRow.key;

    const row = this.getObject(newData, key);

    if(row) {
      switch (type) {
        case 'structuralSubdivisionList':
          if(!row.children) row.children = [];
          row.children.push({
            key: `newData_${row.key}_${row.children.length}`,
            parent: key,
            objClass: 'structuralSubdivisionList',
            editable: true,
            caseOCD: ''
          });
          break;
        case 'caseList':
          row.children = [];
          this.state.checkedKeys.forEach(key => {
            const branch = this.getObject(this.state.treeData, key);
            if(branch) {
              row.children.push({
                key: `newData_${key}`,
                parent: row.key,
                objClass: 'caseList',
                editable: true,
                // nomenIndex: branch.perechenNodeNumber ? branch.perechenNodeNumber[this.lng] : '',
                // numberOfNomenCases: '',
                // shelfLifeOfPerechen: branch.shelfLifeOfPerechen && branch.shelfLifeOfPerechen.idRef ?
                //   {value: branch.shelfLifeOfPerechen.idRef, label: branch.shelfLifeOfPerechen.name[this.lng]} : {},
                // nomenCasesNote: branch.perechenNote ? branch.perechenNote[this.lng] : '',
                // perechenNodeList: branch.title,
                // nomenPerechenNode: branch.title ? {label: branch.title, value: key} : {},
              });
            }
          });
          break;
        default: break;
      }

      this.setState({
        data: newData,
        openCard: false
      })
    }
  };

  getObject = (theObject, key) => {
    let result = null;
    if(theObject instanceof Array) {
      for(let i = 0; i < theObject.length; i++) {
        result = this.getObject(theObject[i], key);
        if(result) return result;
      }
    }
    else if(theObject instanceof Object) {
      if(theObject.key == key) {
        return theObject;
      }
      result = this.getObject(theObject.children, key);
    } else return null;
    return result;
  };
  removeObject = (theObject, key) => {
    let result = null;
    if(theObject instanceof Array) {
      for(let i = 0; i < theObject.length; i++) {
        result = this.removeObject(theObject[i], key);
        if(result) {
          // eslint-disable-next-line
          theObject.forEach((item, idx) => {
            if(item.key === result.key) {
              theObject.splice(idx, 1);
              return;
            }
          });
        }
      }
    }
    else if(theObject instanceof Object) {
      if(theObject.key === key) {
        return theObject;
      }
      result = this.removeObject(theObject.children, key);
    } else return null;
    return result;
  };

  renderTreeNodes = (data) => {
    return data.map((item) => {
      if (item.children) {
        return (
          <TreeNode title={item.title} key={item.key} dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode {...item} dataRef={item}/>;
    });
  };
  closeCard = () => {
    this.setState({ openCard: false })
  };
  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }
  renderDateColumns = (text, record, column) => {
    return (
      <EditableDatePicker
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value ? value.format('DD-MM-YYYY') : '', record.key, column)}
      />
    )
  };
  handleChange(value, key, column) {
    const newData = this.state.data.slice();
    const target = this.getObject(newData, key);
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  stopPropagation = e => {
    e.stopPropagation();
  };
  save = key => {
    return e => {
      e.stopPropagation();
      const newData = this.state.data.slice();
      const target = this.getObject(newData, key);

      if (target) {
        const { parent, editable, key, objClass, ...rest } = target;

        if(objClass === 'structuralSubdivisionList') {
          const name = {};
          SYSTEM_LANG_ARRAY.forEach(lang => {
            name[lang] = rest.caseOCD
          });
          const cube = {
            cubeSConst: 'CubeForAF_Inv',
            doConst: 'doForInv',
            dpConst: 'dpForInv'
          };
          const obj = {
            name,
            fullName: name,
            clsConst: objClass,
            parent: parent.split('_')[1]
          };
          if(target.key.startsWith('newData')) {
            const hideCreateObj = message.loading('CREATING_NEW_OBJECT', 0);
            createObj(cube, obj)
              .then(res => {
                hideCreateObj();
                if(res.success) {
                  target.key = res.data.idItemDO;
                  message.success('OBJECT_CREATED_SUCCESSFULLY');
                  delete target.editable;
                  this.setState({ data: newData });
                }
              }).catch(err => {
              hideCreateObj();
              console.log(err);
            });
          } else {
            this.onSaveCubeData({cube, obj}, {}, target.key, {name})
              .then(resp => {
                if(resp.success) {
                  delete target.editable;
                  this.setState({ data: newData });
                }
              }).catch(err => {
              console.log(err);
            });
          }

        } else {
          const name = {};
          SYSTEM_LANG_ARRAY.forEach(lang => {
            name[lang] = rest.cases
          });
          const cube = {
            cubeSConst: 'CubeForAF_Case',
            doConst: 'doForCase',
            dpConst: 'dpForCase'
          };
          const obj = {
            name,
            fullName: name,
            clsConst: objClass,
            parent: parent.split('_')[1]
          };
          if(target.key.startsWith('newData')) {
            const hideCreateObj = message.loading('CREATING_NEW_OBJECT', 0);
            createObj(cube, obj)
              .then(res => {
                hideCreateObj();
                if(res.success) {
                  target.key = res.data.idItemDO;
                  this.filters = null;
                  this.onSaveCubeData({cube, obj}, rest, res.data.idItemDO, {})
                    .then(resp => {
                      if(resp.success) {
                        delete target.editable;
                        this.setState({ data: newData });
                      }
                    })
                }
              }).catch(err => {
              hideCreateObj();
              console.log(err);
            });
          } else {
            this.onSaveCubeData({cube, obj}, rest, target.key, {})
              .then(resp => {
                if(resp.success) {
                  delete target.editable;
                  this.setState({ data: newData });
                }
              }).catch(err => {
              console.log(err);
            });
          }
        }
      }
    }
  };
  remove = key => {
    const newData = this.state.data.slice();
    this.removeObject(newData, key);
    this.setState({ data: newData });
  };
  cancel = key => {
    const newData = this.state.data.slice();
    if(key.includes('newData')) {
      this.removeObject(newData, key);
      this.setState({data: newData, selectedRow: {} });
      return;
    }
    const target = this.getObject(newData, key);
    if (target) {
      delete target.editable;
      this.setState({ data: newData, selectedRow: {} });
    }
  };
  edit = key => {
    return e => {
      e.stopPropagation();
      const newData = this.state.data.slice();
      const target = this.getObject(newData, key);

      if (target) {
        target.editable = true;
        this.setState({ data: newData });
      }
    }
  };

  loadOptions = c => {
    return () => {
      if(!this.props[c + 'Options']) {
        this.props.getPropVal(c)
      }
    }
  };

  onRowClick = record => {
    this.setState({ selectedRow: record });
  };

  openArticles = () => {
    const fd = new FormData();
    // const nomenPerechenValue = this.state.formData.nomenPerechen.value;
    // fd.append('parent', nomenPerechenValue);
    getObjList(fd)
      .then(res => {
        if(res.success) {
          this.setState({ loading: false, treeData: res.data.map(o => ({
            key: o.id,
            title: o.name[this.lng],
            isLeaf: !o.hasChild
          })) })
        } else {
          console.log(res);
          this.setState({ loading: false, openCard: false })
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ loading: false, openCard: false })
      });
    this.setState({ openCard: true, cardLoading: true })
  };
  onLoadData = (treeNode) => {
    if(treeNode.props.dataRef.children) {
      return Promise.resolve({success: true})
    }
    const fd = new FormData();
    fd.append('parent', treeNode.props.dataRef.key);
    return getObjList(fd)
      .then(res => {
        if(res.success) {
          treeNode.props.dataRef.children = res.data.map(o => ({
            key: o.id,
            title: o.name[this.lng],
            isLeaf: !o.hasChild,
            // perechenNodeNumber: o.perechenNodeNumber,
            // perechenNote: o.perechenNote,
            // shelfLifeOfPerechen: o.shelfLifeOfPerechen
          }));
          this.setState({
            treeData: [...this.state.treeData],
          });
          return {success: true}
        }
      })
      .catch(err => {
        console.log(err);
      })
  };

  /*createNewObj = (objVerData, values, objData) => {
    const hideCreateObj = message.loading('CREATING_NEW_OBJECT', 0);
    return createObj(objVerData.cube, objVerData.obj)
      .then(res => {
        hideCreateObj();
        if(res.success) {

          this.filters = {
            filterDOAnd: [
              {
                dimConst: objVerData.cube.doConst,
                concatType: "and",
                conds: [
                  {
                    ids: res.data.idItemDO
                  }
                ]
              }
            ]
          };
          Promise.all([
            this.onSaveCubeData(
              {cube: {cubeSConst: CUBE_FOR_FUND_AND_IK, doConst: DO_FOR_FUND_AND_IK, dpConst: DP_FOR_FUND_AND_IK}},
              {"nomen": [{value: res.data.idItemDO.split('_')[1], mode: 'ins'}]},
              this.props.match.params.id,
              {}
            ),
            this.onSaveCubeData(objVerData, values, res.data.idItemDO, objData)
          ]).then(resArr => {
            if(resArr.every(res => res.success)) {
              const { dimObjNomen, dimPropNomen } = this.props.tofiConstants;
              this.setState({
                loading: false,
                formData: values,
                ln: parseCube_new(
                  this.props.cubeSNomen['cube'],
                  [],
                  'dp',
                  'do',
                  this.props.cubeSNomen[`do_${dimObjNomen.id}`],
                  this.props.cubeSNomen[`dp_${dimPropNomen.id}`],
                  `do_${dimObjNomen.id}`,
                  `dp_${dimPropNomen.id}`)[0]
              })
            }
          }).catch(err => {
            console.log(err);
          });
        }
      })
  };*/
  onSaveCubeData = (objVerData, values, doItemProp, objDataProp) => {
    let datas = [];
    try {
      datas = [{
        own: [{doConst: objVerData.cube.doConst, doItem: doItemProp, isRel: "0", objData: objDataProp }],
        props: map(values, (val, key) => {
          console.log(key);
          const propMetaData = getPropMeta(this.props[objVerData.cube.cubeSConst]["dp_" + this.props.tofiConstants[objVerData.cube.dpConst].id], this.props.tofiConstants[key]);
          console.log(propMetaData);
          let value = val;
          if((propMetaData.typeProp === 315 || propMetaData.typeProp === 311 || propMetaData.typeProp === 317) && typeof val === 'string') value = {kz: val, ru: val, en: val};
          if(typeof val === 'object' && val.value) value = String(val.value);
          if(typeof val === 'object' && val.mode) propMetaData.mode = val.mode;
          if(propMetaData.isUniq === 2 && val[0].value) {
            propMetaData.mode = val[0].mode;
            value = val.map(v => String(v.value)).join(",");
          }
          return {propConst: key, val: value, typeProp: String(propMetaData.typeProp), periodDepend: String(propMetaData.periodDepend), isUniq: String(propMetaData.isUniq), mode: propMetaData.mode }
        }),
        periods: [{ periodType: '0', dbeg: '1800-01-01', dend: '3333-12-31' }]
      }];
    } catch(err) {
      console.log(err);
      return Promise.reject();
    }
    const hideLoading = message.loading('UPDATING_PROPS', 0);
    return this.props.updateCubeData(objVerData.cube.cubeSConst, moment().format('YYYY-MM-DD'), JSON.stringify(datas))
      .then(res => {
        hideLoading();
        if(res.success) {
          message.success('PROPS_SUCCESSFULLY_UPDATED');
          if(this.filters) {
            this.setState({loading: true});
            return this.props.getCube(objVerData.cube.cubeSConst, JSON.stringify(this.filters))
              .then(() => {
                this.setState({loading: false});
                return {success: true}
              })
          } else {
            return {success: true}
          }
        } else {
          message.error('PROPS_UPDATING_ERROR');
          if(res.errors) {
            res.errors.forEach(err => {
              message.error(err.text);
            });
            return {success: false}
          }
        }
      })
  };

  componentDidMount() {
    this.props.getCube('cubeForAF');
  }

  render() {
    if(isEmpty(this.props.tofiConstants)) return null;
    const { openCard, data, treeData } = this.state;
    const { t, invTypeOptions, invCaseSystemOptions, tofiConstants, tofiConstants: { caseNumber, caseDbeg, caseDend, caseNumberOfPages, caseOCD, fundIndex, caseNotes } } = this.props;

    this.lng = localStorage.getItem('i18nextLng');
    return (
      <div className="LegalEntitiesInventoriesMain">
        <div className="LegalEntitiesInventoriesMain__property">
          <LegalEntitiesInventoryPropsForm
            tofiConstants={tofiConstants}
            lng={this.lng}
            createNewObj={this.createNewObj}
            t={t}
            invTypeOptions={invTypeOptions}
            invCaseSystemOptions={invCaseSystemOptions}
            loadOptions={this.loadOptions}
          />
        </div>
        <div className="LegalEntitiesInventoriesMain__table">
          <div className="LegalEntitiesInventoriesMain__table__heading">
            <div className="table-header">
              <Button onClick={this.addNew}>{t('ADD')}</Button>
              <Button onClick={() => this.addNewChild('firstLevel')} disabled={isEmpty(this.state.selectedRow)}>{t('ADD_FIRST_LEVEL_CHILD')}</Button>
              <Button onClick={() => this.setState({ openCard: true })} disabled={isEmpty(this.state.selectedRow)}>{t('ADD_SECOND_LEVEL_CHILD')}</Button>
            </div>
          </div>
          <div className="LegalEntitiesInventoriesMain__table__body">
            <Table
              columns={[
                {
                  key: 'caseNumber',
                  title: caseNumber.name[this.lng],
                  dataIndex: 'caseNumber',
                  width: '15%',
                  render: (value, rec) => {
                    const obj = {
                      children: value,
                      props: {}
                    };
                    if(rec.rowType === 'secondLevel') {
                      obj.children = this.renderColumns(value, rec, 'caseNumber')
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseIndex',
                  title: fundIndex.name[this.lng],
                  dataIndex: 'caseIndex',
                  width: '5%',
                  render: (value, rec) => {
                    const obj = {
                      children: this.renderColumns(value, rec, 'caseIndex'),
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    }
                    return obj;
                  }
                },
                {
                  key: 'cases',
                  title: 'Заголовок дела',
                  dataIndex: 'cases',
                  width: '21%',
                  render: (value, rec) => {
                    const obj = {
                      children: '',
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    } else {
                      obj.children = this.renderColumns(value, rec, 'cases')
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseDbeg',
                  title: caseDbeg.name[this.lng],
                  dataIndex: 'caseDbeg',
                  width: '10%',
                  render: (value, rec) => {
                    const obj = {
                      children: this.renderDateColumns(value, rec, 'caseDbeg'),
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseDend',
                  title: caseDend.name[this.lng],
                  dataIndex: 'caseDend',
                  width: '10%',
                  render: (value, rec) => {
                    const obj = {
                      children: this.renderDateColumns(value, rec, 'caseDend'),
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseNumberOfPages',
                  title: caseNumberOfPages.name[this.lng],
                  dataIndex: 'caseNumberOfPages',
                  width: '5%',
                  render: (value, rec) => {
                    const obj = {
                      children: this.renderColumns(value, rec, 'caseNumberOfPages'),
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseOCD',
                  title: caseOCD.name[this.lng],
                  dataIndex: 'caseOCD',
                  width: '9%',
                  render: (value, rec) => {
                    const obj = {
                      children: value,
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.children = this.renderColumns(value, rec, 'caseOCD');
                      obj.props.colSpan = 7;
                    }
                    return obj;
                  }
                },
                {
                  key: 'caseNotes',
                  title: caseNotes.name[this.lng],
                  dataIndex: 'caseNotes',
                  width: '20%',
                  render: (value, rec) => {
                    const obj = {
                      children: this.renderColumns(value, rec, 'caseNotes'),
                      props: {}
                    };
                    if(rec.rowType === 'firstLevel') {
                      obj.props.colSpan = 0;
                    }
                    return obj;
                  }
                },
                {
                  key: 'action',
                  title: 'action',
                  dataIndex: 'a',
                  width: '5%',
                  render: (text, record) => {
                    const { editable } = record;
                    const disable = false;
                    return (
                      <div className="editable-row-operations">
                        {
                          editable ?
                            <span>
                              <a onClick={this.save(record.key)} disabled={disable}><Icon type="check"/></a>
                              <Popconfirm title="Отменить?" onConfirm={() => this.cancel(record.key)}>
                                <a style={{marginLeft: '5px'}} onClick={this.stopPropagation}><Icon type="close"/></a>
                              </Popconfirm>
                            </span>
                          : <span>
                              <a><Icon type="edit" style={{fontSize: '14px'}} onClick={this.edit(record.key)}/></a>
                              <Popconfirm title={this.props.t('CONFIRM_REMOVE')} onConfirm={() => this.remove(record.key)}>
                                <a style={{color: '#f14c34', marginLeft: '10px', fontSize: '14px'}} onClick={this.stopPropagation}><Icon type="delete" className="editable-cell-icon"/></a>
                              </Popconfirm>
                            </span>
                        }
                      </div>
                    );
                  },
                },
              ]}
              size="small"
              bordered
              dataSource={data}
              onRowClick={this.onRowClick}
              scroll={{y: '100%'}}
              rowClassName={record => this.state.selectedRow.key === record.key ? 'row-selected' : ''}
            />
            <CSSTransition
              in={openCard}
              timeout={300}
              classNames="card"
              unmountOnExit
            >
              <SiderCardLegalEntities closer={<Button type='danger' onClick={this.closeCard} shape="circle" icon="close"/>} >
                <Tree
                  checkable
                  loadData={this.onLoadData}
                  onCheck={this.onCheck}
                  checkedKeys={this.state.checkedKeys}
                >
                  {this.renderTreeNodes(treeData)}
                </Tree>
                <div className="ant-form-btns">
                  <Button onClick={() => this.addNewChild('secondLevel')}>Сформировать</Button>
                </div>
              </SiderCardLegalEntities>
            </CSSTransition>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    invTypeOptions: state.generalData.invType,
    invCaseSystemOptions: state.generalData.invCaseSystem
  }
}

export default connect(mapStateToProps, {getPropVal})(LegalEntitiesInventoriesMain);