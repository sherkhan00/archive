import React from 'react';

class SiderCardLegalEntities extends React.Component {

  render() {
    const { closer, children } = this.props;

    return (
      <div className="card">
        {closer}
        {children}
      </div>
    )
  }
}

export default SiderCardLegalEntities;
