import React from 'react';
import {Table, Popconfirm, Button, Icon} from 'antd';
import {isEmpty} from 'lodash';
import {Link} from 'react-router-dom';

class LegalEntitiesInventoriesSmall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          key: 'first',
          invNumber: '1',
          invList: '2344',
          systematization: 'По периодам',
          nd: '1991 года'
        },
        {
          key: 'second',
          invNumber: '2',
          invList: '6544',
          systematization: 'По структурным подразделениям',
          nd: '2005 года'
        }
      ]
    };
  }

  editClick = key => {
    return () => {
      console.log(key)
    }
  };
  remove = key => {
    const newData = this.state.data.filter(item => item.key !== key);
    this.setState({ data: newData });
  };

  stopPropagation = e => {
    e.stopPropagation();
  };
  renderInvTableHeader = () => {
    return (
      <div className="flex">
        <Link to="/sourcing/sourcesMaintenance/legalEntities/123/inventories"><Button
          style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '10px'}}
          type="primary"
          shape='circle'
          icon='plus'
        /></Link>
      </div>
    )
  };
  render() {
    if(isEmpty(this.props.tofiConstants)) return null;
    const { t, tofiConstants: {invNumber} } = this.props;
    const lng = localStorage.getItem('i18nextLng');
    return <div className="LegalEntitiesInventoriesSmall">
      <h3 style={{ fontSize: '14px', fontWeight: 'bold', textAlign: 'center', margin: '5px 0'}}>Описи</h3>
      <Table
        bordered
        columns={[
          {
            key: 'invNumber',
            title: invNumber.name[lng],
            dataIndex: 'invNumber',
            width: '10%',
          }, {
            key: 'invList',
            title: t('NAME'),
            dataIndex: 'invList',
            width: '25%',
          }, {
            key: 'systematization',
            title: 'Систематизация дел',
            dataIndex: 'systematization',
            width: '35%',
          }, {
            key: 'nd',
            title: 'НД',
            dataIndex: 'nd',
            width: '15%',
          }, {
            key: 'action',
            title: '',
            dataIndex: '',
            width: '15%',
            render: (text, record) => {
              return (
                <div className="editable-row-operations">
                  <a><Icon type="edit" style={{fontSize: '14px'}} onClick={this.editClick(record.key)}/></a>
                  <Popconfirm title={this.props.t('CONFIRM_REMOVE')} onConfirm={() => this.remove(record.key, record.table)}>
                    <a style={{color: '#f14c34', marginLeft: '10px', fontSize: '14px'}} onClick={this.stopPropagation}><Icon type="delete" className="editable-cell-icon"/></a>
                  </Popconfirm>
                </div>
              );
            },
          }
        ]}
        dataSource={this.state.data}
        size='small'
        title={this.renderInvTableHeader}
        pagination={false}
        scroll={{y: 350}}
        onRowClick={this.handleRowClick}
        style={{height: 'auto', minHeight: 'unset', marginLeft: '5px', marginBottom: '30px'}}
      />
    </div>
  }
}

export default LegalEntitiesInventoriesSmall;